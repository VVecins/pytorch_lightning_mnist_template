import tensorboardX
import datetime
import numpy as np
import argparse
import random


parser = argparse.ArgumentParser(description='Model trainer')

parser.add_argument('-name', type=str, default='mnist_test_run')
parser.add_argument('-model', type=str, default='model_mnist_2_densenet')
parser.add_argument('-datasource', type=str, default='datasource_1_mnist')
parser.add_argument('-loss_function', type=str, default='cross_entropy')
parser.add_argument('-optimizer', type=str, default='adam')
parser.add_argument('-gpus', type=int, default=1)

parser.add_argument('-data_workers', type=int, default=0)
parser.add_argument('-batch_size', type=int, default=32)
parser.add_argument('-learning_rate', type=float, default=1e-5)

parser.add_argument('-checkpoint_metric', type=str, default='val_loss')

parser.add_argument('-fast_dev_run', default=False, type=lambda x: (str(x).lower() == 'true'))

args, args_other = parser.parse_known_args()

write_path = 'C:\\Users\\vecin\\Documents\\PycharmProjects\\pytorch_lightning_mnist_template'
writer = tensorboardX.SummaryWriter(write_path)

metrics = {
    'acc': random.random(),
    'loss': random.random()
}
# writer.add_hparams(vars(args), metrics)

for idx in range(10):
    metrics = {
        'acc': random.random(),
        'loss': random.random()
    }
    writer.add_hparams(vars(args), metrics, global_step=idx)
    writer.add_scalar('acc', metrics['acc'], idx)
    writer.add_scalar('loss', metrics['loss'], idx)

writer.close()