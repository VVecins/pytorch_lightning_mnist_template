from pytorch_lightning.loggers import TensorBoardLogger
from pytorch_lightning.utilities import rank_zero_only
from torch.utils.tensorboard.summary import hparams
from torch import Tensor


class CustomTensorBoardLogger(TensorBoardLogger):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    @rank_zero_only
    def log_hyperparams(self, params: dict, metrics: dict = None) -> None:
        pass

    @rank_zero_only
    def log_hyperparams_metrics(self, params: dict, metrics: dict = None, step: int = 0) -> None:
        params = self._convert_params(params)
        self.hparams.update(params)

        # format params into the suitable for tensorboard
        params = self._flatten_dict(params)
        params = self._sanitize_params(params)

        if metrics:
            self.log_metrics(metrics, step)
            exp, ssi, sei = hparams(params, metrics)
            writer = self.experiment._get_file_writer()
            writer.add_summary(exp)
            writer.add_summary(ssi)
            writer.add_summary(sei)
        self.save()
