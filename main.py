import os
import torch
import torch.nn.functional as F
import argparse
from torch.utils.data import DataLoader
from pytorch_lightning.callbacks import EarlyStopping
from pytorch_lightning.metrics.functional import accuracy
from pytorch_lightning.callbacks import ModelCheckpoint
from pytorch_lightning import loggers as pl_loggers
import pytorch_lightning as pl
from modules.radam import RAdam
from modules.file_utils import FileUtils
from modules.custom_logger import CustomTensorBoardLogger


class MNISTClassifier(pl.LightningModule):

    def __init__(self, args: argparse.Namespace):
        super(MNISTClassifier, self).__init__()
        self.args = args
        self.hparams = args
        self.loss_function = torch.nn.CrossEntropyLoss()
        self.get_loss_function()
        # add for now till CSV logger fixed
        self.custom_log_dict = {}

    # model forward is defined in modules_core class

    def get_loss_function(self):
        """Use loss_function argument to change from default loss function."""
        if self.args.loss_function == 'mse':
            self.loss_function = torch.nn.MSELoss()

    def training_step(self, train_batch, batch_idx):
        x, y = train_batch
        logits = self.forward(x)
        loss = self.loss_function(logits, y)
        acc = accuracy(F.softmax(logits,dim=1), y)
        return {'loss': loss, 'acc': acc}

    def training_epoch_end(self, outputs):
        loss = torch.stack([x['loss'] for x in outputs]).mean()
        acc = torch.stack([x['acc'] for x in outputs]).mean()
        log = {'step': self.current_epoch, 'train_loss': loss, 'train_acc': acc}
        self.custom_log_dict.update(log)
        self.log_dict(self.custom_log_dict, on_epoch=True)
        self.logger.log_hyperparams_metrics(self.hparams, self.custom_log_dict, self.current_epoch)

    def validation_step(self, val_batch, batch_idx):
        x, y = val_batch
        logits = self.forward(x)
        val_loss = self.loss_function(logits, y)
        val_acc = accuracy(F.softmax(logits,dim=1), y)
        return {'val_loss': val_loss, 'val_acc': val_acc}

    def validation_epoch_end(self, outputs):
        val_loss = torch.stack([x['val_loss'] for x in outputs]).mean()
        val_acc = torch.stack([x['val_acc'] for x in outputs]).mean()
        log = {'step': self.current_epoch, 'val_loss': val_loss, 'val_acc': val_acc}
        self.custom_log_dict.update(log)

    def prepare_data(self):
        get_dataset = getattr(__import__('modules_core.' + self.args.datasource, fromlist=['get_dataset']), 'get_dataset')
        self.loader_train, self.loader_val = get_dataset(self.args)

        # todo find cleaner way to do
        # example input array to log model graph
        self.example_input_array = torch.unsqueeze(self.loader_val[0][0], dim=0)

    def train_dataloader(self):
        """Training loop data"""
        return DataLoader(self.loader_train,
                          batch_size=self.args.batch_size,
                          num_workers=self.args.data_workers,
                          shuffle=True,
                          drop_last=True)

    def val_dataloader(self):
        """Validation loop data, called after each epoch"""
        return DataLoader(self.loader_val,
                          batch_size=self.args.batch_size,
                          num_workers=self.args.data_workers,
                          shuffle=False,
                          drop_last=True)

    def configure_optimizers(self):
        if self.args.optimizer.lower() == 'radam':
            return RAdam(self.parameters(), lr=args.learning_rate)
        return torch.optim.Adam(self.parameters(), lr=self.args.learning_rate)


def configure_run(args):
    log_path = os.path.join(os.getcwd(), 'tasks')
    # ceate loggers for second logger pass first logger version, cant find cleaner way to do that
    # tb_logger = pl_loggers.TensorBoardLogger(log_path, name=args.name, log_graph=True, default_hp_metric=False)
    tb_logger = CustomTensorBoardLogger(log_path, name=args.name, log_graph=True, default_hp_metric=False)
    checkpoint_path = os.path.join(tb_logger.log_dir, 'checkpoints')

    FileUtils.createDir(checkpoint_path)
    checkpoint_callback = ModelCheckpoint(
        filepath=checkpoint_path,
        save_top_k=1,
        save_last=True,
        verbose=True,
        monitor=args.checkpoint_metric,
        mode='auto',
        prefix=args.name
    )
    early_stopping = EarlyStopping(
        monitor='val_acc',
        patience=7,
        strict=False
    )
    return tb_logger, checkpoint_callback, early_stopping


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Model trainer')

    parser.add_argument('-name', type=str, default='mnist_test_run')
    parser.add_argument('-model', type=str, default='model_mnist_2_densenet')
    parser.add_argument('-datasource', type=str, default='datasource_1_mnist')
    parser.add_argument('-loss_function', type=str, default='cross_entropy')
    parser.add_argument('-optimizer', type=str, default='adam')
    parser.add_argument('-gpus', type=int, default=1)
    parser.add_argument('-max_epochs', type=int, default=3)

    parser.add_argument('-data_workers', type=int, default=0)
    parser.add_argument('-batch_size', type=int, default=32)
    parser.add_argument('-learning_rate', type=float, default=1e-5)

    parser.add_argument('-checkpoint_metric', type=str, default='val_loss')

    parser.add_argument('-fast_dev_run', default=False, type=lambda x: (str(x).lower() == 'true'))

    args, args_other = parser.parse_known_args()

    # load chosen model and add all model and Trainer specific arguments
    Model = getattr(__import__('modules_core.' + args.model, fromlist=['Model']), 'Model')
    parser = Model.add_model_specific_args(parser)
    parser = pl.Trainer.add_argparse_args(parser)
    args, _ = parser.parse_known_args()

    model = Model(args)

    logger, checkpointer, early_stopping = configure_run(args)

    # EarlyStopping()
    trainer = pl.Trainer.from_argparse_args(
        args,
        default_root_dir='tasks',
        checkpoint_callback=checkpointer,
        early_stop_callback=early_stopping,
        logger=logger,
        limit_train_batches=0.2,
        limit_val_batches=0.2
    )

    trainer.fit(model)
