import torch
import argparse
from main import MNISTClassifier
import torch.nn.functional as F
from argparse import ArgumentParser

class DenseBlock(torch.nn.Module):
    def __init__(self):
        super().__init__()

        self.bn1 = torch.nn.BatchNorm2d(num_features=32)
        self.bn2 = torch.nn.BatchNorm2d(num_features=64)
        self.bn3 = torch.nn.BatchNorm2d(num_features=96)
        self.bn4 = torch.nn.BatchNorm2d(num_features=128)

        # out channels fixed by layer count and each layer out channels, 4 * 32 = 128
        self.conv1 = torch.nn.Conv2d(in_channels=32, out_channels=32, kernel_size=3, stride=1, padding=1)
        self.conv2 = torch.nn.Conv2d(in_channels=64, out_channels=32, kernel_size=3, stride=1, padding=1)
        self.conv3 = torch.nn.Conv2d(in_channels=96, out_channels=32, kernel_size=3, stride=1, padding=1)
        self.conv4 = torch.nn.Conv2d(in_channels=128, out_channels=32, kernel_size=3, stride=1, padding=1)

    def forward(self, x):

        conv1 = self.conv1(self.bn1(F.relu(x)))
        conv2_in = torch.cat([x, conv1], dim=1)

        conv2 = self.conv2(self.bn2(F.relu(conv2_in)))
        conv3_in = torch.cat([x, conv1, conv2], dim=1)

        conv3 = self.conv3(self.bn3(F.relu(conv3_in)))
        conv4_in = torch.cat([x, conv1, conv2, conv3], dim=1)

        conv4 = self.conv4(self.bn4(F.relu(conv4_in)))
        output = torch.cat([x, conv1, conv2, conv3, conv4], dim=1)

        return output


class TransitionLayer(torch.nn.Module):
    def __init__(self, in_channels, out_channels):
        super().__init__()

        self.bn = torch.nn.BatchNorm2d(num_features=out_channels)
        self.conv = torch.nn.Conv2d(in_channels=in_channels, out_channels=out_channels, kernel_size=1, bias=False)
        self.avg_pool = torch.nn.AvgPool2d(kernel_size=2, stride=2, padding=0)

    def forward(self, x):
        bn = self.bn(F.relu(self.conv(x)))
        out = self.avg_pool(bn)

        return out


class Model(MNISTClassifier):
    @staticmethod
    def add_model_specific_args(parent_parser):
        '''Add any model specific arguments.'''
        parser = ArgumentParser(parents=[parent_parser], add_help=False)
        return parser

    def __init__(self, args: argparse.Namespace):
        super().__init__(args)

        self.conv1 = torch.nn.Conv2d(in_channels=1, out_channels=32, kernel_size=3, stride=1, padding=1, bias=False)

        self.denseblock1 = DenseBlock()
        self.denseblock2 = DenseBlock()
        self.denseblock3 = DenseBlock()

        self.transitionLayer1 = TransitionLayer(in_channels=160, out_channels=32)
        self.transitionLayer2 = TransitionLayer(in_channels=160, out_channels=32)
        self.transitionLayer3 = TransitionLayer(in_channels=160, out_channels=32)

        # Classifier
        self.linear = torch.nn.Linear(288, 10)

    def forward(self, x):
        out = self.conv1(x)

        out = self.denseblock1(out)
        out = self.transitionLayer1(out)

        out = self.denseblock2(out)
        out = self.transitionLayer2(out)

        out = self.denseblock3(out)
        out = self.transitionLayer3(out)

        out = out.view(-1, 32*3*3)
        out = self.linear(out)
        out = F.softmax(out, dim=1)
        return out