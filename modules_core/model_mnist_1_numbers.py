import torch
import argparse
from main import MNISTClassifier
import torch.nn as nn
from argparse import ArgumentParser


class Model(MNISTClassifier):

    @staticmethod
    def add_model_specific_args(parent_parser):
        '''Add any model specific arguments.'''
        parser = ArgumentParser(parents=[parent_parser], add_help=False)
        parser.add_argument('--encoder_layers', type=int, default=12)
        return parser

    def __init__(self, args: argparse.Namespace):
        super(Model, self).__init__(args)
        self.conv_env = torch.nn.Sequential(
            nn.Conv2d(in_channels=1, out_channels=4, kernel_size=9, padding=1, stride=1),
            nn.ReLU(),
            nn.BatchNorm2d(num_features=4),
            nn.Conv2d(in_channels=4, out_channels=8, kernel_size=4, padding=2, stride=2),
            nn.ReLU(),
            nn.BatchNorm2d(num_features=8),
            nn.Conv2d(in_channels=8, out_channels=10, kernel_size=4, padding=2, stride=2),
            nn.ReLU(),
            nn.BatchNorm2d(num_features=10),
            nn.AdaptiveAvgPool2d(output_size=(1,1))
        )

    def forward(self, x):
        x = self.conv_env(x)
        x = x.view(x.shape[0], -1)
        return x

    # # Any lightning hook can be overridden
    # # If using different type of model
    # def training_step(self, train_batch, batch_idx):
    #     pass
    #     # do something different?