import os

from torchvision import datasets, transforms
from torchvision.datasets import MNIST


def get_dataset(args):
    transform=transforms.Compose([transforms.ToTensor(),
                                  transforms.Normalize((0.1307,), (0.3081,))])

    dataset_train = MNIST(os.getcwd(), train=True, download=True, transform=transform)
    dataset_test = MNIST(os.getcwd(), train=False, download=True, transform=transform)

    return dataset_train, dataset_test